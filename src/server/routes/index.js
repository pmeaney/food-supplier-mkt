var express = require('express');
var router = express.Router();

router.get('/', (req, res) => {
  res.render('webcontent/langingPage_homePage_example', 
  {
    env: process.env
  })
})

/* GET initial dashboard. */
router.get('/initialDashboard', (req, res) => {
  res.render('pages/initialDashboard', 
  {
    env: process.env
  })
})

module.exports = router;