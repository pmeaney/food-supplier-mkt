import React, { Component } from "react";
import Mapbox from './Mapbox'

class ThingToPutInView extends Component {
  constructor() {
    super();

    this.state = {
    };

  }

  someFunctionWeDefine(msg) {
    console.log('msg received into fn:', msg )
  }

  render() {
    return (
      <div>
          <Mapbox />
      </div>
    );
  }
}

export default ThingToPutInView;